#!/bin/bash

source src/tc.common/bin/tc.common

arch=`uname -m`
os=`uname -s`
target=`echo "$os-$arch" | awk '{ print tolower($0) }'`

tc.info "target: [${target}]"

tc.verbose_exec "rm -rf ${target}"
tc.verbose_exec "mkdir -p ${target}"

for dir in `find src -type d -name "tc.*"`
do
    tc.verbose_exec "cp ${dir}/bin/* ${target}/"
done
