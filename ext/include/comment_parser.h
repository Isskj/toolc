#ifndef _TCSTRING_H
#define _TCSTRING_H

typedef struct {
    void (*e_start_comment)();
    void (*e_end_comment)();
    void (*e_text)(int, char);
} comment_parser;

comment_parser* comment_parser_create();
void comment_parser_handle(comment_parser *parser, char ch);


#endif
