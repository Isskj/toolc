#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "comment_parser.h"

typedef enum
{
    none, single_line, multi_line
} comment_type;

static comment_type type;
static int comment;
static int string;
static char prev;

comment_parser* comment_parser_create()
{
    type = none;
    comment = string = 0;
    prev = '\0';
    return (comment_parser*)malloc(sizeof(comment_parser));
}


void comment_parser_handle(comment_parser *parser, char ch)
{
    if (ch == '\'' || ch == '"') {
        if (string) {
            string = 0;
        } else if (prev == '\0' || prev == '\n' || prev == ' ' || prev == '(') {
            string = 1;
        }
    }

    if (!string && !comment && prev == '/' && ch == '*') {
        type = multi_line;
        comment = 1;
        parser->e_start_comment();
        prev = ch;
        return;
    }

    if (!string && comment && prev == ' ' && ch == '*') {
        prev = ch;
        return;
    }

    if (!string && comment && prev == '*' && ch == '*') {
        prev = ch;
        return;
    }

    if (!string && comment && prev == '*' && ch == '/') {
        type = none;
        comment = 0;
        parser->e_end_comment();
        prev = ch;
        return;
    }

    if (!string && !comment && prev == '/' && ch == '/') {
        type = single_line;
        comment = 1;
        parser->e_start_comment();
        prev = ch;
        return;
    }

    if ((prev == '\0' && ch == '/') ||
            (prev == '\n' && ch == '/') ||
            (prev == ' ' && ch == '/')) {
        prev = ch;
        return;
    }

    if (type == single_line && ch == '\n') {
        type = none;
        comment = 0;
        parser->e_end_comment();
        prev = ch;
        return;
    }

    parser->e_text(comment, ch);
    prev = ch;
}

