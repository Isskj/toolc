#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "comment_parser.h"


int show_usage(const char *program)
{
    printf("\
%s [filename]\n\
    Retrieve comment fields.\n\
", program);
    return 1;
}

static void e_start_comment()
{
}

static void e_end_comment()
{
    fputc('\n', stdout);
}

static void e_text(int comment, char ch)
{
    if (comment) fputc(ch, stdout);
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        return show_usage(argv[0]);
    }

    FILE *fp;
    fp = fopen(argv[1], "r");
    if (fp == NULL) {
        perror(argv[0]);
        return 1;
    }

    comment_parser *parser = comment_parser_create();
    parser->e_start_comment = &e_start_comment;
    parser->e_end_comment = &e_end_comment;
    parser->e_text = &e_text;

    char ch;
    while ((ch = fgetc(fp)) > 0) {
        comment_parser_handle(parser, ch);
    }
    fclose(fp);

    return 0;
}
