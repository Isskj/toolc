build clean:
	make -C src/tc.get_comment $@
	make -C src/tc.make_doc_class $@
	make -C src/tc.common $@
	make -C src/tc.trim_comment $@
	make -C src/tc.trim_space $@
	make -C src/tc.xml_tag $@

dist: build
	./dist.sh

